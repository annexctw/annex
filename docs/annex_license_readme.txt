﻿Annex: Conquer the World credits/license.

Data files are licensed mainly under CC-by-Sa (see "cc-by-sa-3.0-unported.txt"
in the "docs"), for more details read the following text.

The human-readable summary of cc-by-sa-3.0:
Under the following terms: Attribution — You must give appropriate credit,
provide a link to the license, and indicate if changes were made. You may do
so in any reasonable manner, but not in any way that suggests the licensor
endorses you or your use; ShareAlike — If you remix, transform, or build
upon the material, you must distribute your contributions under the same
license as the original.
You are free to: Share — copy and redistribute the material in any medium or
format; Adapt — remix, transform, and build upon the material for any purpose,
even commercially.


******
*Art:*
******

Sand, Road, and Metal floor tiles are made by Adrian Delpha,
under CC-by-Sa license.

Dirt and Grass floor tiles derived from textures made by Tucho Fernandez,
under CC-by-Sa license.

All other art assets including menu, characters, viehicles, buildings, trees,
mountains, and grass in "Annex: Conquer the World" are © 2011 Adrian Delpha,
under the terms of the Creative Commons Attribution-ShareAlike (CC-by-Sa)
3.0 license, or any later version. In brief, this means you are free to share
this work and modify it under the conditions that you provide proper credit to
the author (see instructions below) and license any derivative works under the
same or similar license. To view the full text of the license, visit
"http://creativecommons.org/licenses/by-sa/3.0/" or send a letter to Creative
Commons, 171 Second Street, Suite 300, San Francisco, California, 94105 USA.

You are free to use models as long as proper credit is given, and you abide by
the rules of CC-by-Sa (refer to above), and please Include a link to Annex
(http://www.moddb.com/mods/annex-conquer-the-world) or my portfolio
(http://delphadesign.daportfolio.com/)


********
*Music:*
********

Music obtained through Jamendo http://www.jamendo.com and are under
CC-by-Sa license

Menu music:
"Bomb Alert" by Gregoire Lourme (CC by Sa)
https://www.jamendo.com/en/track/1010827/bomb-alert

End game loss:
"Aftermath" by Gregorie Lourme (CC by Sa)
https://www.jamendo.com/en/track/989073/aftermath-drama

End game win:
"Robot Supremacy"  by Gregorie Lourme (CC by Sa)
https://www.jamendo.com/en/track/973255/robot-supremacy-action


In game:
"Prologue" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076983/prologue

"No Return" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076989/no-return

"Overdrive" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076985/overdrive

"The Clash" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076988/the-clash

"Deathmatch" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1077002/deathmatch

"Black" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1077001/black


********
*Sounds*
********

Various Guns, Cannons, Rockets, and Explosions Provided by Michael Bethencourt.
(CC by Sa)


********
*Voice*
********

Male Voices: by Levi18

http://freesound.org/people/Levi18/sounds/136396/  Under CC0 License

Female Voices: by Charlotte Duckeet  Under CC by SA License

Female Voice Recordings Copyright 2012 Charlotte Duckett
<https://catalog.librivox.org/people_public.php?peopleid=7315>
Editing Copyright 2012 Iwan Gabovitch  <http://qubodup.net>
(Applies to only part of the files, see readme.txt files in archive.)


*******
*Maps:*
*******

-Annex Maps:
Corner City; Great Desert Sm; Head On; Indus; Longroad; Platform; Shallow River;
Tight Urban; Hidden Airbase; Void;  Urban Rumble; Delta; Penisula; Forest;
Great Desert Md; Island Rivers (inspired by Glest map 4 Rivers); Swamp;
Wetlands; In The Mountains; Canyon Run; Island Chain; Fortress; Plantation;
Hunting Grounds; Donut; Island; Corner City; Killing Fields; Bridges

Annex maps are made by Adrian Delpha under CC-by-Sa license.
If used please credit (see instructions below)

-Converted MegaGlest Maps
Conflict - by ttsmj
Fracture - by Wicow
Valley of Death - Glest Team (Original Glest Map)
One on One - Glest Team (Original Glest Map)
River_Crossing - Glest Team (Original Glest Map)
Island Siege- Glest Team (Original Glest Map)
flankenangriff - by Atze
loggerheads_a2 - by Atze


***********
*MegaGlest*
***********

http://megaglest.org

Titus Tscharntke(titi) - Lead developer and creator of the new Mega-Glest
project (http://www.titusgames.de)

Mark Vejvoda(SoftCoder) - Developer of the new Mega-Glest
project (http://www.soft-haus.com)


**************************
*Glest Game 3.2.1 License*
**************************

Except where otherwise noted, all of the documentation, multimedia and software
included in the Glest Game setup package is copyrighted by The Glest Team.

Copyright (C) 2001-2009 The Glest Team. All rights reserved.

This software is provided without any express or implied warranty. In no
event shall the author be held liable for any damages arising from the use
of this software.

This software may be redistributed freely, but all redistributions must retain
all occurences of the above copyright notice and web site addresses that are
currently in place.

email: contact_game@glest.org
web: http://glest.org/

The Glest Team:

Programmer: Martiño Figueroa
Sound artist: Jose Luis González
2D and 3D artist: Tucho Fernández
2D artist and web: José Zanni
SDL port: Matze Braun
Animation: Félix Menéndez
3D artist: Marcos Caruncho


********
*Source*
********

Annex: Conquer the World is built useing an unmotified version of Megaglest
3.9.1, which is under General Public Licence, GPL3, And the source may be
found at:
https://github.com/MegaGlest/megaglest-source/tree/3.9.1

Please see "readme.compiling.windows.txt" and "readme.compiling.linux"
for further information.
