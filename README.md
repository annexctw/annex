[Annex: Conquer the World](http://annexconquer.com) is a libre software cross
platform real-time strategy game.

[![logo](https://bitbucket.org/annexctw/annex/downloads/annex_logo.png)](http://annexconquer.com)

Annex: Conquer the World is a full game based on the
[MegaGlest engine](https://github.com/MegaGlest/megaglest-source),
that brings fast paced combat with a diverse arsenal. Play as one of four Factions:
The East Ocean Alliance, NEO Republic, Renegades, or the Shadow Organization,
and fight for dominance over the world, competing for a priceless red mineral.
The focus of the game are multiplayer and single-player skirmishes. However there are
also scripted scenarios/missions! Currently, no plans are in place for campaigns
or story mode, however this may change.

[**Annex Downloads**](http://annexconquer.com/downloads/)

[![intro](https://bitbucket.org/annexctw/annex/downloads/annex3_trailer_teaser.png)](https://bitbucket.org/annexctw/annex/downloads/annex3_trailer.mp4)
