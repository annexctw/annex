How to make an installer:

--------------------------------
I. GUI Method.
----
1. Download and install stable (the lower/older) version of NSIS (http://nsis.sourceforge.net/Download).
2. If you don't have it yet, download the binary and unpack it in ...os_dependent/windows/???_binary.
3. Run 'NSIS Menu' and choose 'Compile NSI scripts'.
4. Select 'Tools' > 'Settings'.
5. In the 'Symbol name' box write 'APVER'.
6. In the 'Value (optional)' box write number of Annex's version, which is equal to git's tag
  if current commit is tagged (e.g. 4.0); or is equal to "sum" of strings: I. value of parametter
  'CURRENT_DEV_VERSION' (without quotes, e.g. 4.0_dev) written in ../annex_version.txt, and
  II. short hash of git's head commit (e.g. 31f0520), for example: 4.0_dev_31f0520.
7. Select 'Add >>' and 'OK'.
8. Select 'Load Script...' and choose .nsi script.
9. Wait.

--------------------------------
II. Command line, script method.
----
1. (the same work as in GUI Method - 1.).
2. (the same work as in GUI Method - 2.).
3. Find where exactly 'makensis.exe' is installed and remember path to it.
4. Find out the "number" of current version of .nsi installer (similar like in GUI Method - 6.).
5. Go to folder where this Readme is.
6. Write whole command like this:
  "<path to exe>\makensis.exe" /DAPVER=<version> nsis\Annex_installer.nsi
7. Wait.
...
8. Feel free to write some universal Windows' script ^ for that job ;).
