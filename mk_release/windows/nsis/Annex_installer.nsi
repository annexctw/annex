;----------------------------------------------------------------
; 2014 Written by filux <heross(@@)o2.pl>
; Copyright (c) 2014 under GNU GPL v3.0+
;--------------------------------
; written for stable release of nsis and for winxp+,
; but the most important target is win8 family
;----------------------------------------------------------------
!verbose 3
!ifndef APVER
	!define APVER _dev
!endif
;----------------------------------------------------------------
!include "MUI2.nsh"
!include "LogicLib.nsh"
!include "x64.nsh"
!define APNAME Annex
!define APNAME_S annex
!define APNAME_DIR "Annex CtW"
!define APNAME_P_DIR "Annex_CtW"
!define APNAME_REG "${APNAME_DIR}"
!define APNAME_FULL "Annex - Conquer the World"
!define AP_UNINST "uninstall.exe"
!define APVERSION "v${APVER}"
Name "${APNAME} ${APVERSION}"
InstallDir "$PROGRAMFILES\${APNAME_DIR}"
SetCompressor /FINAL /SOLID lzma
SetCompressorDictSize 64
InstProgressFlags smooth colored
!define ROOT_DIR ..\..\..
!addPluginDir "plugins"
OutFile "${APNAME}-installer-windows-32bit-${APVERSION}.exe"
Icon "${ROOT_DIR}\os_dependent\windows\shared\${APNAME_S}.ico"
UninstallIcon "icons\${APNAME_S}-uninstall.ico"
!define ANNEX_HEADER_BMP "..\..\linux\mojosetup\annex-installer\meta\${APNAME_S}_header.bmp"
ShowInstDetails show
ShowUninstDetails show
RequestExecutionLevel admin
;----------------------------------------------------------------
!define MUI_ICON "${ROOT_DIR}\os_dependent\windows\shared\${APNAME_S}.ico"
!define MUI_UNICON "icons\${APNAME_S}-uninstall.ico"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${ANNEX_HEADER_BMP}"
!define MUI_HEADERIMAGE_UNBITMAP "${ANNEX_HEADER_BMP}"
!define MUI_HEADERIMAGE_RIGHT
!define MUI_ABORTWARNING
!define MUI_ABORTWARNING_CANCEL_DEFAULT
!define MUI_UNABORTWARNING
!define MUI_UNFINISHPAGE_NOAUTOCLOSE
;--------------------------------
!insertmacro MUI_PAGE_WELCOME
!define MUI_LICENSEPAGE_TEXT_TOP "${APNAME} Game License"
!insertmacro MUI_PAGE_LICENSE "${ROOT_DIR}\docs\MG_docs\gnu_gpl_3.0.txt"
!define MUI_LICENSEPAGE_TEXT_TOP "${APNAME} Data License"
!insertmacro MUI_PAGE_LICENSE "${ROOT_DIR}\docs\${APNAME_S}_license_readme.txt"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_REBOOTLATER_DEFAULT
!define MUI_FINISHPAGE_LINK ">>> Getting started guide <<<"
!define MUI_FINISHPAGE_LINK_LOCATION "$INSTDIR\manual\index.html"
!define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\docs\MG_docs\README.txt"
!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!define MUI_FINISHPAGE_RUN_TEXT "Run the ${APNAME} game"
!define MUI_FINISHPAGE_RUN "$INSTDIR\${APNAME_S}.exe"
!define MUI_FINISHPAGE_RUN_NOTCHECKED
!insertmacro MUI_PAGE_FINISH
;--------------------------------
!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_COMPONENTS
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH
;----------------------------------------------------------------
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Afrikaans"
!insertmacro MUI_LANGUAGE "Albanian"
!insertmacro MUI_LANGUAGE "Arabic"
!insertmacro MUI_LANGUAGE "Basque"
!insertmacro MUI_LANGUAGE "Belarusian"
!insertmacro MUI_LANGUAGE "Bosnian"
!insertmacro MUI_LANGUAGE "Breton"
!insertmacro MUI_LANGUAGE "Bulgarian"
!insertmacro MUI_LANGUAGE "Catalan"
!insertmacro MUI_LANGUAGE "Croatian"
!insertmacro MUI_LANGUAGE "Czech"
!insertmacro MUI_LANGUAGE "Danish"
!insertmacro MUI_LANGUAGE "Dutch"
!insertmacro MUI_LANGUAGE "Esperanto"
!insertmacro MUI_LANGUAGE "Estonian"
!insertmacro MUI_LANGUAGE "Farsi"
!insertmacro MUI_LANGUAGE "Finnish"
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "Galician"
!insertmacro MUI_LANGUAGE "German"
!insertmacro MUI_LANGUAGE "Greek"
!insertmacro MUI_LANGUAGE "Hebrew"
!insertmacro MUI_LANGUAGE "Hungarian"
!insertmacro MUI_LANGUAGE "Icelandic"
!insertmacro MUI_LANGUAGE "Indonesian"
!insertmacro MUI_LANGUAGE "Irish"
!insertmacro MUI_LANGUAGE "Italian"
!insertmacro MUI_LANGUAGE "Japanese"
!insertmacro MUI_LANGUAGE "Korean"
!insertmacro MUI_LANGUAGE "Kurdish"
!insertmacro MUI_LANGUAGE "Latvian"
!insertmacro MUI_LANGUAGE "Lithuanian"
!insertmacro MUI_LANGUAGE "Luxembourgish"
!insertmacro MUI_LANGUAGE "Macedonian"
!insertmacro MUI_LANGUAGE "Malay"
!insertmacro MUI_LANGUAGE "Mongolian"
!insertmacro MUI_LANGUAGE "Norwegian"
!insertmacro MUI_LANGUAGE "NorwegianNynorsk"
!insertmacro MUI_LANGUAGE "Polish"
!insertmacro MUI_LANGUAGE "PortugueseBR"
!insertmacro MUI_LANGUAGE "Portuguese"
!insertmacro MUI_LANGUAGE "Romanian"
!insertmacro MUI_LANGUAGE "Russian"
!insertmacro MUI_LANGUAGE "SerbianLatin"
!insertmacro MUI_LANGUAGE "Serbian"
!insertmacro MUI_LANGUAGE "SimpChinese"
!insertmacro MUI_LANGUAGE "Slovak"
!insertmacro MUI_LANGUAGE "Slovenian"
!insertmacro MUI_LANGUAGE "SpanishInternational"
!insertmacro MUI_LANGUAGE "Spanish"
!insertmacro MUI_LANGUAGE "Swedish"
!insertmacro MUI_LANGUAGE "Thai"
!insertmacro MUI_LANGUAGE "TradChinese"
!insertmacro MUI_LANGUAGE "Turkish"
!insertmacro MUI_LANGUAGE "Ukrainian"
!insertmacro MUI_LANGUAGE "Uzbek"
!insertmacro MUI_LANGUAGE "Welsh"
;----------------------------------------------------------------
; http://nsis.sourceforge.net/WORKAROUND:_Winx64_Shortcut_Icon_Bug
!verbose push
!verbose 0
!define ___lnkX64IconFix___
!define lnkX64IconFix `!insertmacro _lnkX64IconFix`
!macro _lnkX64IconFix _lnkPath
	!verbose push
	!verbose 0
	${If} ${RunningX64}
		DetailPrint "WORKAROUND: 64bit OS Detected, Attempting to apply lnkX64IconFix"
		Push "${_lnkPath}"
		Call lnkX64IconFix
	${EndIf}
	!verbose pop
!macroend
;--------------------------------
Function lnkX64IconFix ; _lnkPath
	Exch $5
	Push $0
	Push $1
	Push $2
	Push $3
	Push $4
	System::Call 'OLE32::CoCreateInstance(g "{00021401-0000-0000-c000-000000000046}",i 0,i 1,g "{000214ee-0000-0000-c000-000000000046}",*i.r1)i'
	${If} $1 <> 0
		System::Call '$1->0(g "{0000010b-0000-0000-C000-000000000046}",*i.r2)'
		${If} $2 <> 0
			System::Call '$2->5(w r5,i 2)i.r0'
			${If} $0 = 0
				System::Call '$1->0(g "{45e2b4ae-b1c3-11d0-b92f-00a0c90312e1}",*i.r3)i.r0'
				${If} $3 <> 0
					System::Call '$3->5(i 0xA0000007)i.r0'
					System::Call '$3->6(*i.r4)i.r0'
					${If} $0 = 0 
						IntOp $4 $4 & 0xffffBFFF
						System::Call '$3->7(ir4)i.r0'
						${If} $0 = 0 
							System::Call '$2->6(i0,i0)'
							DetailPrint "WORKAROUND: lnkX64IconFix Applied successfully"
						${EndIf}
					${EndIf}
					System::Call $3->2()
				${EndIf}
			${EndIf}
			System::Call $2->2()
		${EndIf}
		System::Call $1->2()
	${EndIf} 
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Pop $0
FunctionEnd
!verbose pop
;----------------------------------------------------------------
Function .onInit
	; http://nsis.sourceforge.net/Shortcuts_removal_fails_on_Windows_Vista
	UserInfo::GetAccountType
	pop $0
	${If} $0 != "admin"
		    MessageBox mb_iconstop "Due to 'Windows' bugs, administrator rights are required."
		    SetErrorLevel 740
		    Quit
	${EndIf}
	;--------------------------------
	${If} ${RunningX64}
		SetRegView 64
	${EndIf}
	;--------------------------------
	InitPluginsDir
	SetOutPath '$PLUGINSDIR'
	File "${ANNEX_HEADER_BMP}"
	;--------------------------------
	ReadRegStr $R0 HKLM "Software\${APNAME_REG}" "Install_Dir"
	ReadRegStr $R1 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}" "UninstallString"
	ReadRegStr $R2 HKLM "Software\${APNAME_REG}" "Version"
	StrCmp $R2 "" 0 +2
		StrCpy $R2 "?"
	StrCmp $R1 "" 0 foundInst
	StrCmp $R0 "" 0 foundInst2
	;--------------------------------
	IfFileExists "$INSTDIR\${AP_UNINST}" 0 +3
	StrCpy $R0 "$INSTDIR"
	IfFileExists "$INSTDIR\${AP_UNINST}" foundInst2 0
	goto doneInit
	;--------------------------------
	foundInst:
		MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION "${APNAME} $R2 is already \
			installed. $\n$\nClick `Ok` to remove the previous installation \
			or `Cancel` to quit." IDOK uninstInit IDCANCEL quitInit
	;--------------------------------
	foundInst2:
		MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION "${APNAME} $R2 is already \
			installed in [$R0]. $\n$\nClick `Ok` to remove the previous \
			installation or `Cancel` to quit." IDOK uninstInit2 IDCANCEL quitInit
	;--------------------------------
	quitInit:
		Quit
	;--------------------------------
	uninstInit:
		ClearErrors
		Exec "$R1"
		Quit
	;--------------------------------
	uninstInit2:
		ClearErrors
		Exec "$R0\${AP_UNINST}"
		Quit
	;--------------------------------
	doneInit:
FunctionEnd
;----------------------------------------------------------------
Function un.onInit
	UserInfo::GetAccountType
	pop $0
	${If} $0 != "admin"
		    MessageBox mb_iconstop "Due to 'Windows' bugs, administrator rights are required."
		    SetErrorLevel 740
		    Quit
	${EndIf}
	;--------------------------------
	${If} ${RunningX64}
		SetRegView 64
	${EndIf}
	;--------------------------------
	InitPluginsDir
FunctionEnd
;----------------------------------------------------------------
Section "${APNAME} game"
	SectionIn RO
	SetDetailsPrint both
	;--------------------------------
	!macro Copy_Directory parm1 parm2 parm3
		SetDetailsPrint none
		CreateDirectory "${parm2}"
		SetDetailsPrint textonly
		DetailPrint ""
		SetDetailsPrint listonly
		SetOutPath "${parm2}"
		DetailPrint "- ...\${parm3}\*.*"
		SetDetailsPrint none
		File /r "${parm1}\*.*"
		SetDetailsPrint both
	!macroend
	;--------------------------------
	SetOutPath "$INSTDIR"
	AccessControl::GrantOnFile "$INSTDIR" "(S-1-5-32-545)" "FullAccess"
	;--------------------------------
	File /r /x ${APNAME_S}_e*bit.7z /x .gitignore "${ROOT_DIR}\os_dependent\windows\x86_binary\*.*"
	File /r /x glest-dev.ini /x .gitignore "${ROOT_DIR}\os_dependent\windows\shared\*.*"
	File /x glest.ini "${ROOT_DIR}\*.ini"
	File "${ROOT_DIR}\*.ico"
	;--------------------------------
	!insertmacro Copy_Directory "${ROOT_DIR}\data" "$INSTDIR\data" "data"
	!insertmacro Copy_Directory "${ROOT_DIR}\docs" "$INSTDIR\docs" "docs"
	!insertmacro Copy_Directory "${ROOT_DIR}\manual" "$INSTDIR\manual" "manual"
	!insertmacro Copy_Directory "${ROOT_DIR}\maps" "$INSTDIR\maps" "maps"
	!insertmacro Copy_Directory "${ROOT_DIR}\scenarios" "$INSTDIR\scenarios" "scenarios"
	!insertmacro Copy_Directory "${ROOT_DIR}\tilesets" "$INSTDIR\tilesets" "tilesets"
	#!insertmacro Copy_Directory "${ROOT_DIR}\tutorials" "$INSTDIR\tutorials" "tutorials"
	!insertmacro Copy_Directory "${ROOT_DIR}\techs" "$INSTDIR\techs" "techs"
	;--------------------------------
	WriteRegStr HKLM "Software\${APNAME_REG}" "Install_Dir" "$INSTDIR"
	AccessControl::GrantOnRegKey HKLM "Software\${APNAME_REG}" "(S-1-5-32-545)" "FullAccess"
	WriteRegStr HKLM "Software\${APNAME_REG}" "Version" "${APVERSION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}" \
		"DisplayName" "${APNAME} ${APVERSION}"
	AccessControl::GrantOnRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}" \
		"(S-1-5-32-545)" "FullAccess"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}" \
		"UninstallString" '"$INSTDIR\${AP_UNINST}"'
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}" "NoRepair" 1
	WriteUninstaller "${AP_UNINST}"
SectionEnd
;----------------------------------------------------------------
Section "Start Menu Shortcuts"
	CreateDirectory "$APPDATA\${APNAME_P_DIR}"
	AccessControl::GrantOnFile "$APPDATA\${APNAME_P_DIR}" "(S-1-5-32-545)" "FullAccess"
	SetOutPath "$APPDATA"
	StrCpy $R9 "$APPDATA"
	SetShellVarContext all
	CreateDirectory "$SMPROGRAMS\${APNAME_FULL}"
	AccessControl::GrantOnFile "$SMPROGRAMS\${APNAME_FULL}" "(S-1-5-32-545)" "FullAccess"
	;--------------------------------
	CreateShortCut "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - User Data.lnk" \
		"$R9\${APNAME_P_DIR}" "" "" 0 "" "" "This folder contains downloaded data \
		(such as mods) and your personal ${APNAME} configuration."
	${lnkX64IconFix} "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - User Data.lnk"
	SetOutPath "$INSTDIR"
	CreateShortCut "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - Uninstaller.lnk" "$INSTDIR\${AP_UNINST}" \
		"" "$INSTDIR\${AP_UNINST}" 0 "" "" "Game uninstaller."
	${lnkX64IconFix} "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - Uninstaller.lnk"
	CreateShortCut "$SMPROGRAMS\${APNAME_FULL}\${APNAME} ${APVERSION}.lnk" "$INSTDIR\${APNAME_S}.exe" \
		"" "$INSTDIR\${APNAME_S}.ico" 0 "" "" "Strategy game."
	${lnkX64IconFix} "$SMPROGRAMS\${APNAME_FULL}\${APNAME} ${APVERSION}.lnk"
	CreateShortCut "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - Map Editor.lnk" \
		"$INSTDIR\${APNAME_S}_editor.exe" "" "$INSTDIR\editor.ico" 0 "" "" "Game tool."
	${lnkX64IconFix} "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - Map Editor.lnk"
	SetOutPath "$INSTDIR\manual"
	CreateShortCut "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - Guide.lnk" \
		"$INSTDIR\manual\index.html" "" "$INSTDIR\manual\index.html" 0 "" "" "Getting started guide."
	${lnkX64IconFix} "$SMPROGRAMS\${APNAME_FULL}\${APNAME} - Guide.lnk"
	SetShellVarContext current
SectionEnd
;----------------------------------------------------------------
section "Tweaks"
	AccessControl::GrantOnRegKey HKLM "Software\Microsoft\Windows NT\CurrentVersion\Multimedia\SystemProfile" \
		"(S-1-5-32-545)" "FullAccess"
	WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Multimedia\SystemProfile" \
		"NetworkThrottlingIndex" 0xffffffff
sectionEnd
;----------------------------------------------------------------
Section "un.${APNAME} game"
	SectionIn RO
	SetDetailsPrint both
	;--------------------------------
	!macro Remove_Directory parm1 parm2
		IfFileExists "${parm1}" 0 +8
		SetDetailsPrint textonly
		DetailPrint ""
		SetDetailsPrint listonly
		DetailPrint "- ...\${parm2}"
		SetDetailsPrint none
		RMDir /r "${parm1}"
		SetDetailsPrint both
	!macroend
	;--------------------------------
	Delete "$INSTDIR\${APNAME_S}*.exe"
		;--------------------------------
		; looks like madness but progress is muuuch more linear
		!insertmacro Remove_Directory "$INSTDIR\techs\base_battle" "techs\base_battle"
		!insertmacro Remove_Directory "$INSTDIR\techs\double_damage_d" "techs\double_damage_d"
		!insertmacro Remove_Directory "$INSTDIR\techs\double_damage_d" "techs\half_damage_h"
		;--------------------------------
	Delete "$INSTDIR\${APNAME_S}*.ico"
	Delete "$INSTDIR\editor.ico"
	Delete "$INSTDIR\glest*.ini"
		;--------------------------------
		!insertmacro Remove_Directory "$INSTDIR\techs\pre_deployed" "techs\pre_deployed"
		!insertmacro Remove_Directory "$INSTDIR\techs\pre_deployed_d" "techs\pre_deployed_d"
		!insertmacro Remove_Directory "$INSTDIR\techs\pre_deployed_h" "techs\pre_deployed_h"
		!insertmacro Remove_Directory "$INSTDIR\techs\mode_assault_d" "techs\mode_assault_d"
		;--------------------------------
	Delete "$INSTDIR\servers.ini"
	Delete "$INSTDIR\7z.*"
	Delete "$INSTDIR\libvlc*.dll"
		;--------------------------------
		!insertmacro Remove_Directory "$INSTDIR\techs\ready_for_war" "techs\ready_for_war"
		!insertmacro Remove_Directory "$INSTDIR\techs\ready_for_war_d" "techs\ready_for_war_d"
		!insertmacro Remove_Directory "$INSTDIR\techs\ready_for_war_h" "techs\ready_for_war_h"
		!insertmacro Remove_Directory "$INSTDIR\techs\mode_assault" "techs\mode_assault"
		;--------------------------------
	IfFileExists "$INSTDIR\OpenAL*.dll" 0 +2
	Delete "$INSTDIR\OpenAL*.dll"
	IfFileExists "$INSTDIR\openal*.dll" 0 +2
	Delete "$INSTDIR\openal*.dll"
	Delete "$INSTDIR\*.log"
	!insertmacro Remove_Directory "$INSTDIR\plugins" "plugins"
		;--------------------------------
		!insertmacro Remove_Directory "$INSTDIR\techs\unlocked" "techs\unlocked"
		!insertmacro Remove_Directory "$INSTDIR\techs\unlocked_d" "techs\unlocked_d"
		!insertmacro Remove_Directory "$INSTDIR\techs\unlocked_h" "techs\unlocked_h"
		!insertmacro Remove_Directory "$INSTDIR\techs\mode_assault_h" "techs\mode_assault_h"
		;--------------------------------
	!insertmacro Remove_Directory "$INSTDIR\lua" "lua"
	!insertmacro Remove_Directory "$INSTDIR\data" "data"
	!insertmacro Remove_Directory "$INSTDIR\docs" "docs"
		;--------------------------------
		!insertmacro Remove_Directory "$INSTDIR\techs\simple_deployed" "techs\simple_deployed"
		!insertmacro Remove_Directory "$INSTDIR\techs\simple_deployed_d" "techs\simple_deployed_d"
		!insertmacro Remove_Directory "$INSTDIR\techs\simple_deployed_h" "techs\simple_deployed_h"
		!insertmacro Remove_Directory "$INSTDIR\techs\tech_lv_1-6" "techs\tech_lv_1-6"
		;--------------------------------
	!insertmacro Remove_Directory "$INSTDIR\manual" "manual"
	!insertmacro Remove_Directory "$INSTDIR\maps" "maps"
	!insertmacro Remove_Directory "$INSTDIR\scenarios" "scenarios"
		;--------------------------------
		!insertmacro Remove_Directory "$INSTDIR\techs\simple_battle" "techs\simple_battle"
		!insertmacro Remove_Directory "$INSTDIR\techs\simple_battle_d" "techs\simple_battle_d"
		!insertmacro Remove_Directory "$INSTDIR\techs\simple_battle_h" "techs\simple_battle_h"
		!insertmacro Remove_Directory "$INSTDIR\techs\tech_lv_1-5" "techs\tech_lv_1-5"
		;--------------------------------
	!insertmacro Remove_Directory "$INSTDIR\screens" "screens"
	!insertmacro Remove_Directory "$INSTDIR\tilesets" "tilesets"
	!insertmacro Remove_Directory "$INSTDIR\tutorials" "tutorials"
		;--------------------------------
		!insertmacro Remove_Directory "$INSTDIR\techs\tech_lv_1-4" "techs\tech_lv_1-4"
		!insertmacro Remove_Directory "$INSTDIR\techs\tech_lv_1-3" "techs\tech_lv_1-3"
		!insertmacro Remove_Directory "$INSTDIR\techs\tech_lv_1-2" "techs\tech_lv_1-2"
		!insertmacro Remove_Directory "$INSTDIR\techs\tech_lv_1" "techs\tech_lv_1"
		;--------------------------------
	!insertmacro Remove_Directory "$INSTDIR\techs" "techs"
	;--------------------------------
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APNAME_REG}"
	DeleteRegKey HKLM "SOFTWARE\${APNAME_REG}"
	;--------------------------------
	SetShellVarContext all
	Delete "$SMPROGRAMS\${APNAME_FULL}\*.*"
	RMDir "$SMPROGRAMS\${APNAME_FULL}"
	SetShellVarContext current
	;--------------------------------
	Delete "$INSTDIR\${AP_UNINST}"
	RMDir "$INSTDIR"
	;--------------------------------
	IfFileExists "$APPDATA\${APNAME_P_DIR}\cache\*.*" 0 +2
	Delete "$APPDATA\${APNAME_P_DIR}\cache\*.*"
	;--------------------------------
SectionEnd
;----------------------------------------------------------------
Section /o "un.Personal settings"
	SetDetailsPrint both
	;--------------------------------
	IfFileExists "$APPDATA\${APNAME_P_DIR}\*.ini" 0 +2
	Delete "$APPDATA\${APNAME_P_DIR}\*.ini"
	IfFileExists "$APPDATA\${APNAME_P_DIR}\*.log" 0 +2
	Delete "$APPDATA\${APNAME_P_DIR}\*.log"
	IfFileExists "$APPDATA\${APNAME_P_DIR}\*.mgg" 0 +2
	Delete "$APPDATA\${APNAME_P_DIR}\*.mgg"
	IfFileExists "$APPDATA\${APNAME_P_DIR}\saved\*.*" 0 +2
	Delete "$APPDATA\${APNAME_P_DIR}\saved\*.*"
SectionEnd
;----------------------------------------------------------------
Section /o "un.All personal files"
	SetDetailsPrint both
	;--------------------------------
	IfFileExists "$APPDATA\${APNAME_P_DIR}" 0 +2
	RMDir /r "$APPDATA\${APNAME_P_DIR}"
SectionEnd
;----------------------------------------------------------------

