-- 2011 Written by Mark Vejvoda - mark_vejvoda@hotmail.com
-- 2014 Rewritten by filux - heross(@@)o2.pl
-- Copyright (c) 2011-2014 under GNU GPL v3.0+

local GAME_INSTALL_SIZE = 550000000;
local GAME_VERSION = "_dev";
local APP_NAME = "Annex";
local APP_NAME_S = "annex";
local APP_NAME_L = "Annex - Conquer the World"
local APP_PERSONAL_DIR = ".annex_ctw";

local APP_PERSONAL_DIR_WP = MojoSetup.info.homedir .. '/' .. APP_PERSONAL_DIR;
local APP_DIR_NAME = APP_NAME_S;
local APP_VERSION = 'v' .. GAME_VERSION;
local APP_COMMENT = APP_NAME .. ' ' .. APP_VERSION;
local APP_UNINSTALLER = 'uninstall-' .. APP_NAME_S .. '.sh';
local _ = MojoSetup.translate

Setup.Package
{
  vendor = "annexconquer.com",
  id = APP_NAME_S,
  description = APP_COMMENT,
  version = APP_VERSION,
  splash = APP_NAME_S .. '_header.bmp',
  superuser = false,
  write_manifest = true,
  support_uninstall = true,
  recommended_destinations =
  {
    MojoSetup.info.homedir,
    "/opt/games",
    "/usr/local/games",
  },

  precheck = function(package)
    local APP_UNINSTALLER_WP_DIR = APP_DIR_NAME .. '/' .. APP_UNINSTALLER;
    local previousPath = '';
    if MojoSetup.platform.exists(MojoSetup.info.homedir .. '/' .. APP_UNINSTALLER_WP_DIR) then
	previousPath = MojoSetup.info.homedir .. '/' .. APP_DIR_NAME
    elseif MojoSetup.platform.exists('/opt/games/' .. APP_UNINSTALLER_WP_DIR) then
	previousPath = '/opt/games/' .. APP_DIR_NAME
    elseif MojoSetup.platform.exists('/usr/local/games/' .. APP_UNINSTALLER_WP_DIR) then
	previousPath = '/usr/local/games/' .. APP_DIR_NAME
    end
    if previousPath ~= '' then
	local uninstallQuestionT = _("Previous installation");
	local uninstallQuestionM = MojoSetup.format(_("Detected a previous installation of %0, do you want to remove it?"), APP_NAME);
	if MojoSetup.promptyn(uninstallQuestionT,'[' .. previousPath .. ']\n\n' .. uninstallQuestionM, true) then
	    os.execute(previousPath .. '/' .. APP_UNINSTALLER)
	    if MojoSetup.platform.exists(APP_PERSONAL_DIR_WP .. '/cache/') then
		os.execute('rm -f ' .. APP_PERSONAL_DIR_WP .. '/cache/*')
	    end
	end
    end
  end,
  postinstall = function(package)
    if MojoSetup.destination ~= '' then
	if MojoSetup.platform.exists(MojoSetup.destination .. '/manual/index.html') then
	    local guideQuestionT = _("Guide");
	    local guideQuestionM = _("Do you want to see the 'Getting Started Guide'?");
	    local gameQuestionT = _("Game");
	    local gameQuestionM = MojoSetup.format(_("Do you want to launch the %0 game now?"), APP_NAME);
	    if MojoSetup.promptyn(guideQuestionT, guideQuestionM) then
		MojoSetup.launchbrowser(MojoSetup.destination .. '/manual/index.html')
	    end
	    if MojoSetup.promptyn(gameQuestionT, gameQuestionM) then
		os.execute('(sleep 7s; ' .. MojoSetup.destination .. '/' .. APP_NAME_S .. ')&')
	    end
	end
    end
  end,

  preuninstall = function(package)
    if MojoSetup.destination ~= '' then
	if MojoSetup.platform.exists(MojoSetup.destination .. '/lib/') then
	    os.execute('rm -rf ' .. MojoSetup.destination .. '/lib/')
	end
	if MojoSetup.platform.exists(MojoSetup.destination .. '/.directory') then
	    os.execute('rm -f ' .. MojoSetup.destination .. '/.directory')
	end
	if MojoSetup.platform.exists(MojoSetup.destination .. '/core') then
	    os.execute('rm -f ' .. MojoSetup.destination .. '/core')
	end
	if MojoSetup.platform.exists(MojoSetup.destination .. '/core.1') then
	    os.execute('rm -f ' .. MojoSetup.destination .. '/core.1')
	end
    end
  end,

  Setup.Eula
  {
    description = MojoSetup.format(_("%0 Game License"), APP_NAME),
    source = 'docs/MG_docs/gnu_gpl_3.0.txt',
  },
  Setup.Eula
  {
    description = MojoSetup.format(_("%0 Data License"), APP_NAME),
    source = 'docs/annex_license_readme.txt',
  },
--  Setup.Readme
--  {
--    description = MojoSetup.format(_("%0 README"), APP_NAME),
--    source = 'docs/MG_docs/README.txt',
--  },

  Setup.Option
  {
    value = true,
    required = true,
    disabled = false,
    bytes = GAME_INSTALL_SIZE,
    description = APP_NAME_L .. " " .. APP_VERSION,
    Setup.File
    {
      source = 'base:///' .. APP_NAME_S .. '_data.tar.xz',
    },

    Setup.DesktopMenuItem
    {
      disabled = false,
      name = APP_NAME_L .. " " .. APP_VERSION,
      genericname = MojoSetup.format(_("Strategy game. %0"), APP_COMMENT),
      tooltip = _("A real time strategy game."),
      builtin_icon = false,
      icon = APP_NAME_S .. '.png',
      commandline = "%0/" .. APP_NAME_S,
      category = "Game;StrategyGame",
    },
    Setup.DesktopMenuItem
    {
      disabled = false,
      name = MojoSetup.format(_("%0 - Map Editor"), APP_NAME),
      genericname = MojoSetup.format(_("Game tool. %0"), APP_COMMENT),
      tooltip = _("A real time strategy game."),
      builtin_icon = false,
      icon = APP_NAME_S .. '_editor.png',
      commandline = "%0/" .. APP_NAME_S .. "_editor",
      category = "Game;StrategyGame",
    },
    Setup.DesktopMenuItem
    {
      disabled = false,
      name = MojoSetup.format(_("%0 - Uninstall"), APP_NAME),
      genericname = MojoSetup.format(_("Game uninstaller. %0"), APP_COMMENT),
      tooltip = _("A real time strategy game."),
      builtin_icon = false,
      icon = APP_NAME_S .. '_uninstall.png',
      commandline = "%0/" .. APP_UNINSTALLER,
      category = "Game;StrategyGame",
    }
  }
}

-- end of config.lua ...
