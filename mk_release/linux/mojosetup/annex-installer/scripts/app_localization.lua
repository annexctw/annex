-- Your contributions are welcome. Please ensure you agree to the terms of the
-- contributors license (CC-BY-SA 3.0 Unported)
--
-- default language "en"

MojoSetup.applocalization = {
    ["Uninstall complete"] = {
        en = "Uninstall complete. You may also delete '.annex_ctw' in your home directory to remove personal settings.",
        de = "Deinstallation abgeschlossen. Um auch Deine Annex-Konfiguration zu entfernen kannst Du jetzt noch das Verzeichnis '.annex_ctw' in Deinem Home-Verzeichnis löschen.",
        it = "Disinstallazione completata. Se desideri cancellare i salvataggi e le impostazioni cancella '.annex_ctw' nella tua directory home.",
        pl = "Usuwanie zakończone. Możesz usunąć '.annex_ctw' z katalogu domowego by usunąć zapisane gry i ustawienia.",
        ro = "Dezinstalare completă. Ai putea șterge de asemenea '.annex_ctw' din directorul tău principal pentru a elimina configurările personale.",
        ru = "Удаление завершено. Вы также можете удалить '.annex_ctw' в Вашей домашней папке, чтобы удалить персональные настройки.",
    };

    ["%0 README"] = {
        de = "%0 README-Datei",
        it = "%0 LEGGIMI",
        pl = "Podstawowe informacje o %0",
        ro = "%0 CITEȘTE-MĂ",
    };

    ["%0 Game License"] = {
        de = "%0-Lizenz (Spiel)",
        es = "Licencia de uso de %0",
        pl = "Licencja gry %0",
        ro = "Licență Joc %0",
        ru = "Игровая лицензия %0",
    };

    ["%0 Data License"] = {
        de = "%0-Lizenz (Daten)",
        pl = "Licencja danych %0",
        ro = "Licență Date %0",
        ru = "Лицензия данных %0",
    };

    ["Previous installation"] = {
        it = "Installazione precedente",
        pl = "Poprzednia instalacja",
    };

    ["Detected a previous installation of %0, do you want to remove it?"] = {
        de = "Es wurde eine frühere Installation von %0 erkannt. Möchtest Du sie entfernen?",
        it = "E' stata trovata una versione meno recente di %0, vuoi rimuoverla?",
        pl = "Wykryto poprzednią instalację %0, czy chcesz ją usunąć?",
        ro = "S-a detectat o instalare anterioară a %0, dorești să o elimini?",
        ru = "Найдена прежде установленная версия %0, хотите удалить её?",
    };

    ["Guide"] = {
        it = "Guida",
        pl = "Poradnik",
    };

    ["Do you want to see the 'Getting Started Guide'?"] = {
        it = "Vuoi vedere la 'Guida per Iniziare'?",
        pl = "Czy chcesz zobaczyć 'Poradnik dla początkujących'?",
    };

    ["Game"] = {
        de = "Spiel",
        es = "Juego",
        it = "Partita",
        pl = "Gra",
        ro = "Joc",
        ru = "Игра",
    };

    ["Do you want to launch the %0 game now?"] = {
        it = "Vuoi lanciare il gioco %0 ora?",
        pl = "Czy chcesz uruchomić teraz grę %0?",
    };

    ["A real time strategy game."] = {
        it = "Un gioco di strategia in tempo reale.",
        pl = "Darmowa gra strategiczna czasu rzeczywistego.",
        ro = "Un joc de strategie în timp real.",
        ru = "Стратегия в реальном времени.",
    };

    ["Strategy game. %0"] = {
        it = "Gioco di strategia. %0",
        pl = "Gra strategiczna. %0",
        ro = "Joc de strategie. %0",
        ru = "Стратегия. %0",
    };

    ["Game tool. %0"] = {
        it = "Strumento di gioco. %0",
        pl = "Narzędzie gry. %0",
        ro = "Instrument de joc. %0",
        ru = "Игровой инструмент. %0",
    };

    ["Game uninstaller. %0"] = {
        it = "Disinstallatore del gioco. %0",
        pl = "Deinstalator gry. %0",
    };

    ["%0 - Map Editor"] = {
        it = "%0 - Editor di mappa",
        pl = "%0 - Edytor Map",
        ro = "%0 - Editor Hartă",
        ru = "%0 - Редактор карт",
    };

    ["%0 - Uninstall"] = {
        it = "%0 - Disinstalla",
        pl = "%0 - Odinstaluj",
        ro = "%0 - Dezinstalare",
        ru = "%0 - Удалить",
    };
};

-- end of app_localization.lua ...
