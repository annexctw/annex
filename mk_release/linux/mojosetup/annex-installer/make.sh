#!/bin/bash
# Use this script to build Annex Installer for a Version Release
# ----------------------------------------------------------------------------
# 2011 Written by Mark Vejvoda <mark_vejvoda@hotmail.com>
# 2014 Rewritten by filux <heross(@@)o2.pl>
# Copyright (c) 2011-2014 under GNU GPL v3.0+

# To install the installer silently you may run it like this:
# ./annex-installer.run --noprompt --i-agree-to-all-licenses --destination /home/softcoder/annex-temp-test --noreadme --ui=stdio

# What may be reguired to install before compilation (Debian's name): libgtk2.0-dev
LANG=C

CURRENTDIR="$(dirname "$(readlink -f "$0")")"
project_root_dir="$(readlink -f "$CURRENTDIR/../../../..")"
REPODIR="$CURRENTDIR/$project_root_dir"
kernel="$(uname -s | tr '[A-Z]' '[a-z]')"
architecture="$(uname -m | tr '[A-Z]' '[a-z]')"
App_name_s="annex"; App_name_l="Annex"; App_name_ll="Annex CtW"
R_VERSION="_dev"; compilation_type="normal"
if [ "$1" != "" ] && [ "$1" != "--debug" ] && [ "$1" != "--repackonly" ] && [ "$2" != "--debug" ] && \
    [ "$2" != "--repackonly" ]; then
    R_VERSION="$1"
    if [ "$2" != "" ]; then INSTALL_SIZE1="$2"; fi
fi
case $architecture in
    "x86_64") a_binary_dir="x86-64_binary";;
    "i386"|"i486"|"i586"|"i686"|"i786"|"i886"|"i986"|"x86") architecture="x86"; a_binary_dir="x86_binary";;
    *) a_binary_dir="???_binary";;
esac
VERSION="v${R_VERSION}"
annex_data_path="$project_root_dir"
annex_linux_path="$project_root_dir/os_dependent/linux/shared"
annex_binary_path="$project_root_dir/os_dependent/linux/$a_binary_dir"
app_installer_bin_name="${App_name_l}-installer-${kernel}-${architecture}-${VERSION}.run"
cd $CURRENTDIR

exitdueerror=0
if [ "$(which gcc 2>/dev/null)" == "" ]; then
    echo "Compiller 'gcc' (build-essential?) DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which g++ 2>/dev/null)" == "" ]; then
    echo "Compiller 'g++' (build-essential?) DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which cmake 2>/dev/null)" == "" ]; then
    echo "Build tool 'cmake' DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which zip 2>/dev/null)" == "" ]; then
    echo "Compression tool 'zip' DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which xz 2>/dev/null)" == "" ]; then
    echo "Compression tool 'xz' (xz-utils?) DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which tar 2>/dev/null)" == "" ]; then
    echo "Compression tool 'tar' DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$exitdueerror" -eq "1" ]; then exit 1; fi

mojo_config_lua="scripts/config.lua"
mojo_config_lua_orig="scripts/config_orig.lua"
if [ -f "$mojo_config_lua" ] && [ -f "$mojo_config_lua_orig" ]; then
    rm -f "$mojo_config_lua"
fi
if [ -f "$mojo_config_lua_orig" ] && [ ! -f "$mojo_config_lua" ]; then
    if [ -f "scripts/.gitignore" ]; then
	cp -f "$mojo_config_lua_orig" "$mojo_config_lua"
    else
	mv "$mojo_config_lua_orig" "$mojo_config_lua"
    fi
fi
if [ -f "$mojo_config_lua" ]; then
    if [ "$INSTALL_SIZE1" != "" ]; then
	INSTALL_SIZE="$(($INSTALL_SIZE1 - $(($(($INSTALL_SIZE1 * 7)) / 100))))"
	prmtr="local GAME_INSTALL_SIZE ="
	sed -i "s/$prmtr .*/$prmtr $INSTALL_SIZE;/" "$mojo_config_lua"
    fi
    if [ "$R_VERSION" != "" ]; then
	prmtr="local GAME_VERSION ="
	sed -i "s/$prmtr .*/$prmtr \"$R_VERSION\";/" "$mojo_config_lua"
    fi
fi

# Below is the name of the archive to create and tack onto the installer.
# *NOTE: The filename's extension is of critical importance as the installer
# does a patch on extension to figure out how to decompress!
app_archivefilename_data="${App_name_s}_data.tar.xz"
app_archivefilename="${App_name_s}_pkg.zip"

echo "About to build installer for $VERSION"

REPACKONLY=0; DEBUG=0
if [ "$1" == "--debug" ] || [ "$2" == "--debug" ]; then
    echo "debug build!"
    DEBUG=1
fi
if [ "$1" == "--repackonly" ] || [ "$2" == "--repackonly" ]; then
    echo "repacking installer only!"
    REPACKONLY=1
fi

CC="$(which gcc)"
CXX="$(which g++)"

OSTYPE="$(uname -s)"
if [ "$OSTYPE" == "Linux" ]; then
    NCPU="$(lscpu -p | grep -cv '^#')"
elif [ "$OSTYPE" == "Darwin" ]; then
    NCPU="$(sysctl -n hw.ncpu)"
elif [ "$OSTYPE" == "SunOS" ]; then
    NCPU="$(/usr/sbin/psrinfo |wc -l |sed -e 's/^ *//g;s/ *$//g')"
else
    NCPU=1
fi
if [ "$NCPU" == "" ] || [ "$NCPU" -eq "0" ]; then NCPU=1; fi

echo "Will use $NCPU core(s). If this is wrong, check NCPU at top of script."

set -e
# Show everything that we do here on stdout.
# set -x

if [ "$DEBUG" -eq "1" ]; then
    LUASTRIPOPT=""
    BUILDTYPE="Debug"
    TRUEIFDEBUG="TRUE"
    FALSEIFDEBUG="FALSE"
else
    LUASTRIPOPT="-s"
    BUILDTYPE="MinSizeRel"
    TRUEIFDEBUG="FALSE"
    FALSEIFDEBUG="TRUE"
fi

# Clean up previous run, build fresh dirs for Base Archive.
rm -rf image ${app_installer_bin_name} ${app_archivefilename}; sleep 1s
mkdir image; sleep 1s
mkdir image/guis
mkdir image/scripts
mkdir image/data
mkdir image/meta

# This next section copies live data from the Annex directories
if [ "$REPACKONLY" -eq "0" ]; then
    rm -rf data; sleep 1s
    mkdir -p data

    INSTALL_ROOTDIR="$CURRENTDIR"
    INSTALLDATADIR="$INSTALL_ROOTDIR/data"

    cd "$CURRENTDIR/icons"
    cp -f --no-dereference --preserve=all *.png "$INSTALLDATADIR"
    cp -R -f --no-dereference --preserve=all $annex_binary_path/* "$INSTALLDATADIR"
    cp -R -f --no-dereference --preserve=all $annex_linux_path/* "$INSTALLDATADIR"
    if [ -f "$INSTALLDATADIR/glest-dev.ini" ]; then rm -f "$INSTALLDATADIR/glest-dev.ini"; fi
    rm -f $INSTALLDATADIR/*.desktop $INSTALLDATADIR/${App_name_s}_e*_linux_*z

    cd "$annex_data_path"
    cp -R -f --no-dereference --preserve=all data maps scenarios techs \
    tilesets docs manual "$INSTALLDATADIR"
    cp -f --no-dereference --preserve=all servers.ini glestkeys.ini editor.ico "$INSTALLDATADIR" || :

    #cd "$INSTALLDATADIR"
    #LIBVLC_DIR_CHECK="$(ldd annex.bin | grep "libvlc\." | sort -u | awk '{print $3}')"
    #f [ "$LIBVLC_DIR_CHECK" != "" ] && [ "$LIBVLC_DIR_CHECK" != "not" ]; then
	#LIBVLC_DIR="$(dirname $LIBVLC_DIR_CHECK)"
    #fi
    #if [ "$LIBVLC_DIR" != "" ]; then
	#echo "LibVLC installed in [$LIBVLC_DIR] copying to lib/"
	#cp -r $LIBVLC_DIR/vlc lib/
	#exit 1
    #fi
fi

cd $CURRENTDIR/..

# Build MojoSetup binaries from scratch.
# YOU ALWAYS NEED THE LUA PARSER IF YOU WANT UNINSTALL SUPPORT!
rm -rf cmake-build; sleep 1s
mkdir cmake-build
cd cmake-build
cmake \
    -DCMAKE_BUILD_TYPE=$BUILDTYPE \
    -DCMAKE_C_COMPILER=$CC \
    -DCMAKE_CXX_COMPILER=$CXX \
    -DMOJOSETUP_MULTIARCH=FALSE \
    -DMOJOSETUP_ARCHIVE_ZIP=TRUE \
    -DMOJOSETUP_ARCHIVE_TAR=TRUE \
    -DMOJOSETUP_ARCHIVE_TAR_BZ2=TRUE \
    -DMOJOSETUP_ARCHIVE_TAR_GZ=TRUE \
    -DMOJOSETUP_ARCHIVE_TAR_XZ=TRUE \
    -DMOJOSETUP_INPUT_XZ=TRUE \
    -DMOJOSETUP_INTERNAL_LIBLZMA=TRUE \
    -DMOJOSETUP_BUILD_LUAC=TRUE \
    -DMOJOSETUP_GUI_GTKPLUS2=TRUE \
    -DMOJOSETUP_GUI_GTKPLUS2_STATIC=TRUE \
    -DMOJOSETUP_GUI_NCURSES=TRUE \
    -DMOJOSETUP_GUI_NCURSES_STATIC=TRUE \
    -DMOJOSETUP_GUI_STDIO=TRUE \
    -DMOJOSETUP_GUI_STDIO_STATIC=TRUE \
    -DMOJOSETUP_GUI_WWW=FALSE \
    -DMOJOSETUP_GUI_WWW_STATIC=FALSE \
    -DMOJOSETUP_LUALIB_DB=FALSE \
    -DMOJOSETUP_LUALIB_IO=TRUE \
    -DMOJOSETUP_LUALIB_MATH=FALSE \
    -DMOJOSETUP_LUALIB_OS=TRUE \
    -DMOJOSETUP_LUALIB_PACKAGE=TRUE \
    -DMOJOSETUP_LUA_PARSER=TRUE \
    -DMOJOSETUP_IMAGE_BMP=TRUE \
    -DMOJOSETUP_IMAGE_JPG=FALSE \
    -DMOJOSETUP_IMAGE_PNG=FALSE \
    -DMOJOSETUP_INTERNAL_BZLIB=TRUE \
    -DMOJOSETUP_INTERNAL_ZLIB=TRUE \
    -DMOJOSETUP_URL_HTTP=FALSE \
    -DMOJOSETUP_URL_FTP=FALSE \
    ..

# Perhaps needed to remove compiler / linker warnings considered as errors
# sed -i 's/-Werror//' Makefile

make -j$NCPU

# Strip the binaries and GUI plugins, put them somewhere useful.
if [ "$DEBUG" -ne "1" ]; then
    strip ./mojosetup
fi

mv ./mojosetup ../${App_name_s}-installer/${app_installer_bin_name}
for feh in *.so *.dll *.dylib ; do
    if [ -f $feh ]; then
        if [ "$DEBUG" -ne "1" ]; then
            strip $feh
        fi
        mv $feh ../${App_name_s}-installer/image/guis
    fi
done

# Compile the Lua scripts, put them in the base archive.
for feh in ../scripts/*.lua ; do
    ./mojoluac $LUASTRIPOPT -o ../${App_name_s}-installer/image/scripts/${feh}c  $feh
done

# Don't want the example config...use our's instead.
rm -f ../${App_name_s}-installer/image/scripts/config.luac
./mojoluac $LUASTRIPOPT -o ../${App_name_s}-installer/image/scripts/config.luac ../${App_name_s}-installer/scripts/config.lua

# Don't want the example app_localization...use our's instead.
rm -f ../${App_name_s}-installer/image/scripts/app_localization.luac
./mojoluac $LUASTRIPOPT -o ../${App_name_s}-installer/image/scripts/app_localization.luac ../${App_name_s}-installer/scripts/app_localization.lua

# Fill in the rest of the Base Archive...
cd $CURRENTDIR

# Compress the main data archive
cd data
if [ -f ../$app_archivefilename_data ]; then rm -f ../$app_archivefilename_data; fi
tar cf - * | xz -9e > ../$app_archivefilename_data

# now remove everything except for the docs folder and the data archive
shopt -s extglob
rm -rf !(docs|$app_archivefilename_data)
cd docs
rm -rf !(annex_license_readme.txt|MG_docs)
cd MG_docs
rm -rf !(gnu_gpl_3.0.txt|README.txt)
cd ../..
mv "../$app_archivefilename_data" "$app_archivefilename_data"
cd $CURRENTDIR

cp -R data/* image/data/
cp meta/* image/meta/

# Need these scripts to do things like install menu items, etc, on Unix.
if [ "$OSTYPE" == "Linux" ] || [ "$OSTYPE" == "SunOS" ]; then
    mkdir image/meta/xdg-utils
    cp ../meta/xdg-utils/* image/meta/xdg-utils/
    chmod a+rx image/meta/xdg-utils/*
fi

echo
if [ "$OSTYPE" == "Darwin" ]; then
    # Build up the application bundle for Mac OS...
    APPBUNDLE="${App_name_ll}.app"
    rm -rf "$APPBUNDLE"
    cp -Rv ../misc/MacAppBundleSkeleton "$APPBUNDLE"
    perl -w -pi -e 's/YOUR_APPLICATION_NAME_HERE/'"${APPBUNDLE}"'/g;' "${APPBUNDLE}/Contents/Info.plist"
    mv ${App_name_s}-installer "${APPBUNDLE}/Contents/MacOS/mojosetup"
    mv image/* "${APPBUNDLE}/Contents/MacOS/"
    rmdir image
    ibtool --compile "${APPBUNDLE}/Contents/Resources/MojoSetup.nib" ../misc/MojoSetup.xib
else
    # Make an archive of the Base Archive dirs and nuke the originals...
    cd image

    # create the compressed image for the installer (this will use zip as a container)
    zip -9r ../${app_archivefilename} *

    cd ..
    rm -rf image
    # Append the archive to the mojosetup binary, so it's "self-extracting."
    ../cmake-build/make_self_extracting ${app_installer_bin_name} ${app_archivefilename}

    rm -f ${app_archivefilename}
fi
rm -rf data

# ...and that's that.
set +e
set +x
echo "Successfully built!"
ls -lha ${app_installer_bin_name}

if [ "$DEBUG" -eq "1" ]; then
    echo
    echo
    echo
    echo 'ATTENTION: THIS IS A DEBUG BUILD!'
    echo " DON'T DISTRIBUTE TO THE PUBLIC."
    echo ' THIS IS PROBABLY BIGGER AND SLOWER THAN IT SHOULD BE.'
    echo ' YOU HAVE BEEN WARNED!'
    echo
    echo
    echo
fi

exit 0
