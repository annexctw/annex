#!/bin/sh
# Use this script to improve configuration of '.desktop' files.
# ----------------------------------------------------------------------------
# 2014 Written by filux <heross(@@)o2.pl>
# Copyright (c) 2014 under GNU GPL v3.0+
LANG=C

CURRENTDIR="$(dirname "$(readlink -f "$0")")"
cd "$CURRENTDIR"; prmtr="Icon="; prmtr2="Exec="
if [ -f "annex.desktop" ] && [ -f "annex.png" ] && [ -f "annex.bin" ] \
    && [ -f "annex" ] && [ ! -f "glest-dev.ini" ]; then
    desktop_location="$CURRENTDIR/annex.desktop"; icon_location="$CURRENTDIR/annex.png"
    exec_location="$CURRENTDIR/annex"
    sed -i -e "s#$prmtr.*#$prmtr$icon_location#" -e "s#$prmtr2.*#$prmtr2$exec_location#" \
	"$desktop_location"
    chmod +x $desktop_location
fi
if [ -f "annex_editor.desktop" ] && [ -f "annex_editor.png" ] && [ -f "annex_editor.bin" ] \
    && [ -f "annex_editor" ] && [ ! -f "glest-dev.ini" ]; then
    desktop_location="$CURRENTDIR/annex_editor.desktop"
    icon_location="$CURRENTDIR/annex_editor.png"; exec_location="$CURRENTDIR/annex_editor"
    sed -i -e "s#$prmtr.*#$prmtr$icon_location#" -e "s#$prmtr2.*#$prmtr2$exec_location#" \
	"$desktop_location"
    chmod +x $desktop_location
fi
