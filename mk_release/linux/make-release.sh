#!/bin/bash
# Use this script to build Annex Archives for a Version Release
# ----------------------------------------------------------------------------
# 2014 Written by filux <heross(@@)o2.pl>
# Copyright (c) 2014 under GNU GPL v3.0+
LANG=C

cleartemp=1; nosnapldata=1
testonhead=0; snapshot=0; binonly=0; dataonly=0; archbasedonly=0; sourceonly=0
embedded=1; helpex=0; noparam=0; installeronly=0; onlylinux_ins=0; onlywindows_ins=0
create_sum_file="no"
while [ "$noparam" -lt "$#" ]; do noparam=$(($noparam + 1))
  case ${@:$noparam:1} in
    "--help") helpex=1;;
    "--releasetest") testonhead=1;;
    "--sourceonly") sourceonly=1;;
    "--archbasedonly") archbasedonly=1;;
    "--installeronly") installeronly=1;;
    "--installeronly-linux") installeronly=1; onlylinux_ins=1;;
    "--installeronly-windows") installeronly=1; onlywindows_ins=1;;
    "--binaryonly") binonly=1;;
    "--dataonly") dataonly=1;;
    *) ;;
  esac
done

if [ "$helpex" -eq "1" ]; then
echo -e "\n Use this script to build Annex Archives for a Version Release.
 Usage: make-release.sh [OPTIONS].\n
> For new release, based on informations from the 'annex_version.txt' and existing git tags
  (use it only inside a updated git repository and with commited changes and for release make
  sure that every repository related with Annex is also updated and is selected master branch).\n
   The most common and recommended method of use:
     'make-release.sh'                  - create archives for a new release or for snapshot;\n
    options available for release:
	'... --releasetest'             - allows a real test of the script on HEAD which allows
					  introduce fixes and commit them before set a new tag,
					  or for creating under supervision 'beta' packages for
					  final tests (don't use it for other purposes);
	'... --sourceonly'              - useful e.g. in situation when user forgot to set
					  a proper tag to some repository;
	'... --archbasedonly'		- create architecture based archives;
	'... --installeronly'		- create installer, additionaly is possible to specify
					  which exactly installer, by using longer version of
					  the parameter e.g. '--installeronly-linux';\n
    options available for snapshot:
	'... --binaryonly'              - create only binary archive;
	'... --dataonly'                - create only data archives;\n
 Exit status: 1 - if an significant error was detected, in other cases 0.\n"
    exit 0
fi

CURRENTDIR="$(dirname "$(readlink -f "$0")")"
VERSION_FILE="$(cat "$CURRENTDIR/../annex_version.txt")"
DEV_VERSION="$(echo "$VERSION_FILE" | awk -F '=' '/CURRENT_DEV_VERSION/ {print $2}' | \
    awk -F '"' '{print $2}')"
VERSION="$(echo "${DEV_VERSION}_$(git rev-parse --short HEAD 2>/dev/null)")"
OLD_VERSION="$(echo "$VERSION_FILE" | awk -F '=' '/OLD_VERSION/ {print $2}' | \
    awk -F '"' '{print $2}')"
ENGINE_VERSION="$(echo "$VERSION_FILE" | awk -F '=' '/ENGINE_VERSION/ {print $2}' | \
    awk -F '"' '{print $2}')"
MAC_ENGINE_VERSION="$(echo "$VERSION_FILE" | awk -F '=' '/ENGINE_MACOS_VERSION/ {print $2}' | \
    awk -F '"' '{print $2}')"
KERNEL="$(uname -s | tr '[A-Z]' '[a-z]')"
ARCHITECTURE="$(uname -m | tr '[A-Z]' '[a-z]')"
if [ "$ARCHITECTURE" == "x86_64" ]; then a_binary_dir="x86-64_binary"; else a_binary_dir="x86_binary"; fi
binary_download_s="wget -c --progress=bar https://bitbucket.org/annexctw/annex-binary/downloads"
git_current_commit="$(git rev-parse HEAD 2>/dev/null)"
if [ "$git_current_commit" != "" ]; then
    tag_detection="$(git show-ref --tags | grep "$git_current_commit")"
    if [ "$tag_detection" == "" ] && [ "$testonhead" -ne "1" ]; then snapshot=1; fi
    if [ "$tag_detection" != "" ]; then create_sum_file="yes"; fi
fi

formatarchive1="tar.xz"; formatarchive2="7z"; formatarchive3="zip"; formatarchive="$formatarchive1"
if [ "$snapshot" -eq "1" ]; then
    embedded=0; testonhead=1; cleartemp=1; archbasedonly=0; sourceonly=0
    installeronly=0; formatarchive="$formatarchive2"; datacomparedto=""
    onlylinux_ins=0; onlywindows_ins=0
    if [ "$binonly" -eq "1" ]; then dataonly=0; fi
else
    binonly=0; dataonly=0; datacomparedto="-v$OLD_VERSION"; XZ_OPT="$XZ_OPT -9e"
    if [ "$archbasedonly" -eq "1" ]; then sourceonly=0; fi
    if [ "$onlylinux_ins" -eq "1" ]; then onlywindows_ins=0; fi
fi

App_name_s="annex"; App_name_l="Annex"; App_name_ll="Annex CtW"
RELEASEDIR_ROOT="$(readlink -f "$CURRENTDIR/../../../${App_name_s}-release")"
SNAPSHOT_RELEASE="${App_name_s}-snapshot"
RELEASENAME1="${App_name_s}-source"
REPODIR1="$CURRENTDIR/../../"
RELEASENAME3="${App_name_s}-data"
RELEASENAME4="${App_name_s}-source-wl_embedded"
RELEASENAME7="${App_name_s}-masterserver"
REPODIR7="$CURRENTDIR/../../../megaglest-masterserver/"
RELEASENAME8="${App_name_s}-data-updates$datacomparedto"
RELEASENAME9="${App_name_s}-binary-linux-x86_64"
RELEASENAME10="${App_name_s}-binary-linux-x86"
RELEASENAME11="${App_name_l}-game-linux-x86_64"
RELEASENAME12="${App_name_l}-game-linux-x86"
RELEASENAME14="${App_name_s}-binary-windows-32bit"
RELEASENAME16="${App_name_l}-game-windows-32bit"
RELEASENAME17="${App_name_s}-binary-macos"
RELEASENAME19="${App_name_l}-game-macos"
RELEASENAME30="${App_name_l}-installer-$KERNEL-$ARCHITECTURE"
RELEASENAME32="${App_name_l}-installer-windows-32bit"

exitdueerror=0
if [ "$(which git 2>/dev/null)" == "" ]; then
    echo "Repository tool 'git' DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which 7za 2>/dev/null)" == "" ]; then
    echo "Compression tool 'p7zip-full' DOES NOT EXIST on this system, please install it."
    exitdueerror=1
fi
if [ "$(which zip 2>/dev/null)" == "" ]; then
    echo "Compression tool 'zip' DOES NOT EXIST on this system, please install it."
    if [ "$testonhead" -eq "1" ]; then exitdueerror=1; fi
fi
if [ "$(which makensis 2>/dev/null)" == "" ]; then
    echo "Installer tool 'nsis' DOES NOT EXIST on this system, please install it."
fi
if [ "$exitdueerror" -eq "1" ]; then exit 1; fi

if [ "$snapshot" -ne "1" ] && [ -d "$RELEASEDIR_ROOT" ]; then
    find "$RELEASEDIR_ROOT" -maxdepth 1 -name ${DEV_VERSION}_* -not -name $VERSION -type d \
	| xargs rm -rf 2>/dev/null
    find "$RELEASEDIR_ROOT" -maxdepth 1 -name *dev_* -type d -mtime +7 | xargs rm -rf 2>/dev/null
    find "$RELEASEDIR_ROOT" -maxdepth 1 -name *mediapreview* -type d -mtime +60 | xargs rm -rf 2>/dev/null
fi

common_4() {
    RELEASEDIR="RELEASEDIR$1"; PACKAGE="PACKAGE$1"
    dosize=0; dnsize=1; workcnt=0; worklmt=50; targetpdspd=0
    (if [ "$2" == "dir" ]; then
	checktarget="${!RELEASEDIR}"; loopslp=10
    else
	checktarget="${!PACKAGE}"; loopslp=20
    fi
    worklmt=$(($((60 / $loopslp)) * 60))
    while [ "$dnsize" != "" ] && [ "$dnsize" -gt "$dosize" ]; do
	sleep "$loopslp"s
	if [ "$(ls -A "$checktarget" 2>/dev/null)" ]; then
	    if [ "$targetpdspd" -eq "0" ]; then
		if [ "$(du -Ls "$checktarget" 2>/dev/null | awk '{print $1;}')" -gt "5" ]; then
		    targetpdspd=1; foundtrgs="$(ls | wc -l 2>/dev/null)"
		fi
	    else
		dosize="$dnsize"; sleep 2s
		dnsize="$(du -Ls "$checktarget" 2>/dev/null | awk '{print $1;}')"
		if [ "$dnsize" != "" ] && [ "$dnsize" -gt "$dosize" ]; then echo -n "|"; fi
		if [ "$dnsize" == "" ] || [ "$(ls | wc -l 2>/dev/null)" -gt "$foundtrgs" ]; then dosize="$dnsize"; fi
	    fi
	else
	    workcnt=$(($workcnt + 1))
	    if [ "$workcnt" -gt "$worklmt" ] || [ "$targetpdspd" -eq "1" ]; then dosize=$dnsize; fi
	fi
    done) &
}

common_3() {
    REPODIR="REPODIR$1"; RELEASE="RELEASE$1"
    cd "${!REPODIR}"
    if [ "$testonhead" -ne "1" ] && [ "$2" != "no" ] && [ "$2" != "check" ]; then
	eval RELEASE$1="$(git tag --contains "$(git rev-list --tags --max-count=1)" | tail -n 1)"
    else
	if [ "$snapshot" -ne "1" ]; then
	    eval RELEASE$1="$VERSION"
	else
	    eval RELEASE$1="snapshot"
	fi
    fi
    if [ "$3" != "" ]; then RELEASE="RELEASE$3"; fi
}

common_2() {
    PACKAGE="PACKAGE$1"; RELEASENAME_FULL="RELEASENAME_FULL$1"; RELEASE="RELEASE$1"
    if [ "$2" != "" ]; then RELEASE="RELEASE$2"; fi
    cd "$RELEASEDIR_ROOT/${!RELEASE}"
    if [ "$cleartemp" -ne "1" ] || [ "$embedded" -eq "1" ] || [ "$snapshot" -eq "1" ]; then
	echo -en "\nCreating ${!PACKAGE} "
    fi
    if [ -f "${!RELEASENAME_FULL}.$formatarchive1" ]; then
	rm -f "${!RELEASENAME_FULL}.$formatarchive1"
    fi
    if [ -f "${!RELEASENAME_FULL}.$formatarchive2" ]; then
	rm -f "${!RELEASENAME_FULL}.$formatarchive2"
    fi
    if [ -f "${!RELEASENAME_FULL}.$formatarchive3" ]; then
	rm -f "${!RELEASENAME_FULL}.$formatarchive3"
    fi
    if [ "$snapshot" -eq "1" ]; then
	if [ -d "$SNAPSHOT_RELEASE" ]; then rm -rf "$SNAPSHOT_RELEASE"; fi
	if [ -d "${!RELEASENAME_FULL}" ]; then mv "${!RELEASENAME_FULL}" "$SNAPSHOT_RELEASE"; fi
	packwhat="$SNAPSHOT_RELEASE"
    else
	packwhat="${!RELEASENAME_FULL}"
    fi
    common_4 "$1"
    if [ "$formatarchive" == "tar.xz" ]; then
	tar cJf "${!RELEASENAME_FULL}.$formatarchive" "$packwhat"
    elif [ "$formatarchive" == "7z" ]; then
	7za a "${!RELEASENAME_FULL}.$formatarchive" "$packwhat" >/dev/null
    elif [ "$formatarchive" == "zip" ]; then
	zip -9r "${!RELEASENAME_FULL}.$formatarchive" "$packwhat" >/dev/null
    fi
    if [ "$snapshot" -eq "1" ] && [ -d "$SNAPSHOT_RELEASE" ]; then
	mv "$SNAPSHOT_RELEASE" "${!RELEASENAME_FULL}"
    fi
}

common_1() {
    RELEASENAME="RELEASENAME$1"; REPODIR="REPODIR$1"
    common_3 "$1" "$2" "$3"
    if [ "$snapshot" -ne "1" ] && [ "$2" != "no" ] && [ "$2" != "check" ] && [ "$archbasedonly" -ne "1" ]; then
	echo -en "\nFetching tags to the ${!RELEASENAME} repository "
	git fetch --tags upstream 2>/dev/null
	if [ "$?" -ne "0" ]; then git fetch --tags --quiet origin; fi
	git fetch upstream 2>/dev/null
	if [ "$?" -ne "0" ]; then git fetch --quiet origin; fi
    fi
    if [ "$3" != "" ]; then RELEASE="RELEASE$3"; fi

    if [ "$snapshot" -ne "1" ]; then
	eval RELEASENAME_FULL$1="${!RELEASENAME}-v${!RELEASE}"
    else
	eval RELEASENAME_FULL$1="${!RELEASENAME}"
    fi
    RELEASENAME_FULL="RELEASENAME_FULL$1"
    eval PACKAGE$1="${!RELEASENAME_FULL}.$formatarchive"
    PACKAGE="PACKAGE$1"
    eval RELEASEDIR$1="$RELEASEDIR_ROOT/${!RELEASE}/${!RELEASENAME_FULL}"
    if [ "$2" != "quiet" ]; then
	if [ "$cleartemp" -ne "1" ] || [ "$embedded" -eq "1" ] || [ "$snapshot" -eq "1" ] || [ "$2" == "check" ]; then
	    echo -en "\nCreating ${!RELEASENAME_FULL} directory "
	else
	    echo -en "\nCreating ${!PACKAGE} "
	fi
    fi

    RELEASEDIR="RELEASEDIR$1"
    if [ -d "${!RELEASEDIR}" ]; then rm -rf "${!RELEASEDIR}"; fi
    if [ "$2" != "check" ]; then
	mkdir -p "${!RELEASEDIR}"
	cd "${!RELEASEDIR}"
	common_4 "$1" "dir"
	if [ "$2" != "no" ]; then
	    if [ "$testonhead" -ne "1" ]; then whatarchive="${!RELEASE}"; else whatarchive="HEAD"; fi
	    git archive --remote "${!REPODIR}" "$whatarchive" | tar x
	fi
    else
	cd "$RELEASEDIR_ROOT/${!RELEASE}"
	if [ -e "${!RELEASENAME_FULL}.$formatarchive1" ] || [ -e "${!RELEASENAME_FULL}.$formatarchive2" ] || \
	    [ -e "${!RELEASENAME_FULL}.$formatarchive3" ]; then
	    common_4 "$1" "dir"
	    if [ -e "${!RELEASENAME_FULL}.tar.xz" ]; then
		tar xJf "${!RELEASENAME_FULL}.tar.xz" -C "./"
	    elif [ -e "${!RELEASENAME_FULL}.7z" ]; then
		7za x "${!RELEASENAME_FULL}.7z" >/dev/null
	    elif [ -e "${!RELEASENAME_FULL}.zip" ]; then
		unzip "${!RELEASENAME_FULL}.zip" >/dev/null
	    fi
	fi
    fi
}

common_3 1
echo -n "All packages are created in directory: $RELEASEDIR_ROOT/${!RELEASE}/"

# --------------------------------------------------------

# Source Code Archive for a Version Release

if [ "$archbasedonly" -ne "1" ]; then
    common_1 1
    if [ "$embedded" -ne "1" ] && [ "$snapshot" -ne "1" ] && [ "$dataonly" -ne "1" ]; then common_2 1; fi
else
    common_1 1 check
    if [ ! -f "${!RELEASENAME_FULL}.$formatarchive1" ] && [ ! -f "${!RELEASENAME_FULL}.$formatarchive2" ]; then
	common_1 1 quiet
    fi
fi

# --------------------------------------------------------

# Data Archive for a Version Release

if [ "$binonly" -ne "1" ] && [ "$sourceonly" -ne "1" ]; then
    common_1 3 no 1
    cd "$RELEASEDIR1"
    # - - - - - - - -
    cp -f --no-dereference --preserve=all servers.ini glestkeys.ini editor.ico ${!RELEASEDIR}
    cp -R -f --no-dereference --preserve=all data docs manual maps scenarios techs \
    tilesets ${!RELEASEDIR}
    # - - - - - - - -
    if [ "$snapshot" -eq "1" ] && [ "$nosnapldata" -eq "1" ] && [ -f "../${!PACKAGE}" ]; then
	if [ -f "../$RELEASENAME8.$formatarchive1" ] || [ -f "../$RELEASENAME8.$formatarchive2" ]; then
	    if [ -f "../$RELEASENAME8.$formatarchive1" ]; then data_update="$RELEASENAME8.$formatarchive1"
	    else data_update="$RELEASENAME8.$formatarchive2"; fi

	    archive_ratio="$(($(du -s "../${!PACKAGE}" | awk '{print $1}') \
		/ $(du -s "../$data_update" | awk '{print $1}')))"
	else
	    archive_ratio=100
	fi

	if [ "$(find "../${!PACKAGE}" -mtime +180)" ] || [ "$archive_ratio" -le "5" ]; then
	    nosnapldata=0
	else
	    if [ -f "../old-${!PACKAGE}" ]; then rm -f "../old-${!PACKAGE}"; fi
	    cp -f --no-dereference --preserve=all "../${!PACKAGE}" "../old-${!PACKAGE}"
	fi
    else
	nosnapldata=0
    fi
#    if [ "$snapshot" -ne "1" ]; then common_2 3 1; fi
fi

# --------------------------------------------------------

# Source Archive wihout embedded libs for a Version Release

if [ "$embedded" -eq "1" ] && [ "$snapshot" -ne "1" ] && [ "$archbasedonly" -ne "1" ] \
    && [ "$installeronly" -ne "1" ]; then
    common_1 4 no 1
    if [ "$binonly" -ne "1" ]; then
	# - - - - - - - -
	cp -R -f --no-dereference --preserve=all $RELEASEDIR1/* ${!RELEASEDIR}
	if [ -d "${!RELEASEDIR}/data/core/misc_textures/flags" ]; then
	    rm -rf "${!RELEASEDIR}/data/core/misc_textures/flags"
	fi
	if [ -d "${!RELEASEDIR}/data/core/fonts" ]; then
	    rm -rf "${!RELEASEDIR}/data/core/fonts"
	fi
	# - - - - - - - -
    fi
    if [ "$archbasedonly" -ne "1" ]; then
	common_2 4 1
    fi
fi

# --------------------------------------------------------

# Masterserver Archive for a Version Release

if [ "$snapshot" -ne "1" ] && [ "$dataonly" -ne "1" ] && [ "$binonly" -ne "1" ] \
    && [ "$archbasedonly" -ne "1" ] && [ "$installeronly" -ne "1" ]; then
    if [ -d "$REPODIR7/.git" ]; then
	common_1 7 yes 1
	common_2 7 1
    else
	echo -en "\nIgnoring $RELEASENAME7 "
    fi
fi

# --------------------------------------------------------

# Data Diff Archive for a Version Release
# This part is/was in:
# 2011 Written by Mark Vejvoda <mark_vejvoda@hotmail.com>
# 2014 Rewritten a little by filux <heross(@@)o2.pl>
# Copyright (c) under GNU GPL v3.0+

if [ "$binonly" -ne "1" ] && [ "$archbasedonly" -ne "1" ] && [ "$snapshot" -eq "1" ] && [ "$installeronly" -ne "1" ]; then
    if [ "$snapshot" -eq "1" ]; then
	OLD_DATA_NAME="old-$RELEASENAME3"
	OLD_DATA_ARCHIVE="$RELEASEDIR_ROOT/snapshot/$OLD_DATA_NAME"
	DATA_ARCHIVE="data-${App_name_s}-new"
	UNPACK_LOCATION="../$OLD_DATA_NAME/"
    else
	OLD_DATA_ARCHIVE="$RELEASEDIR_ROOT/$OLD_VERSION/$RELEASENAME3-v$OLD_VERSION"
	DATA_ARCHIVE="$RELEASENAME3-v$RELEASE3"
	UNPACK_LOCATION="$RELEASEDIR_ROOT/$OLD_VERSION/"
    fi
    if [ -d "$OLD_DATA_ARCHIVE" ] || [ -e "$OLD_DATA_ARCHIVE.$formatarchive1" ] || [ -e "$OLD_DATA_ARCHIVE.$formatarchive2" ]; then
	common_1 8 no 1
	if [ -d "$OLD_DATA_ARCHIVE" ]; then
	    if [ -e "$OLD_DATA_ARCHIVE.$formatarchive1" ] || [ -e "$OLD_DATA_ARCHIVE.$formatarchive2" ]; then
		rm -rf "$OLD_DATA_ARCHIVE"
	    fi
	fi
	if [ "$snapshot" -eq "1" ]; then
	    if [ -d "$OLD_DATA_ARCHIVE" ]; then rm -rf "$OLD_DATA_ARCHIVE"; fi
	    mkdir -p "$OLD_DATA_ARCHIVE"
	fi
	if [ -e "$OLD_DATA_ARCHIVE.tar.xz" ]; then
	    tar xJf "$OLD_DATA_ARCHIVE.tar.xz" -C "$UNPACK_LOCATION" 2>/dev/null
	elif [ -e "$OLD_DATA_ARCHIVE.7z" ]; then
	    7za x "$OLD_DATA_ARCHIVE.7z" "-o$UNPACK_LOCATION" >/dev/null
	fi

	cd ..
	if [ "$snapshot" -eq "1" ]; then
	    if [ -d "$DATA_ARCHIVE" ]; then rm -rf "$DATA_ARCHIVE"; fi
	    sleep 0.25s; mv "$RELEASENAME3" "$DATA_ARCHIVE"

	    if [ -d "$OLD_DATA_NAME/$OLD_DATA_NAME" ]; then rm -rf "$OLD_DATA_NAME/$OLD_DATA_NAME"; fi
	    mv "$OLD_DATA_NAME/$SNAPSHOT_RELEASE" "$OLD_DATA_NAME/$OLD_DATA_NAME"
	    if [ -d "$OLD_DATA_NAME-old" ]; then rm -rf "$OLD_DATA_NAME-old"; fi
	    sleep 0.25s; mv "$OLD_DATA_NAME" "$OLD_DATA_NAME-old"
	    if [ -d "$OLD_DATA_NAME" ]; then rm -rf "$OLD_DATA_NAME"; fi
	    sleep 0.25s; mv "$OLD_DATA_NAME-old/$OLD_DATA_NAME" "$OLD_DATA_NAME"
	    if [ -d "$OLD_DATA_NAME-old" ]; then rm -rf "$OLD_DATA_NAME-old"; fi
	    if [ -f "old-$PACKAGE3" ]; then rm -f "old-$PACKAGE3"; fi
	fi

	diffchanges="$(diff --strip-trailing-cr --no-dereference --brief -r -x "*~" \
	    "$OLD_DATA_ARCHIVE" "$DATA_ARCHIVE")"

	echo "$diffchanges" | while read line; do
	    onlyinpos=$(expr match "$line" "Only in ")
	    linemode=0
	    if [ "$onlyinpos" -eq "8" ]; then
		onlyinpos=$(expr match "$line" "Only in $DATA_ARCHIVE")
		if [ "$onlyinpos" -ge "16" ]; then
		    linemode=1
		    #echo "**NOTE: Found ONLY in new archive... [$line]"
		else
		    linemode=2
		    #echo "**NOTE: FOUND ONLY in old archive... [$line], [match: $onlyinpos]"
		fi
	    else
		linemode=3
		#echo "**NOTE: Found two different versions of the same file... [$line]"
	    fi
	    if [ "$linemode" -eq "1" ] || [ "$linemode" -eq "2" ]; then
		beginofwhat="$DATA_ARCHIVE"
	    else
		beginofwhat="$DATA_ARCHIVE/"
	    fi
	    addfilepos=$(awk "BEGIN {print index(\"$line\", \"$beginofwhat\")}")
	    if [ "$linemode" -eq "1" ] || [ "$linemode" -eq "3" ]; then
		line=${line:$addfilepos-1}
		if [ "$linemode" -eq "1" ]; then
		    line=${line/: //}
		else
		    line=${line/ differ/}
		fi
		line=${line/ is a symbolic link/}
		line=${line/$DATA_ARCHIVE\/}
	    else
		line=""
	    fi

	    if [ -n "$line" ]; then
		destdir="$(dirname "$RELEASEDIR8/$line")"
		mkdir -p "$destdir"
		if [ -d "$DATA_ARCHIVE/$line" ]; then
		    cp -R -f --no-dereference --preserve=all "$DATA_ARCHIVE/$line" "$destdir"
		else
		    cp -f --no-dereference --preserve=all "$DATA_ARCHIVE/$line" "$destdir"
		fi
	    fi
	done

	if [ "$snapshot" -eq "1" ]; then
	    if [ -d "$RELEASENAME3" ]; then rm -rf "$RELEASENAME3"; fi
	    sleep 0.25s; mv "$DATA_ARCHIVE" "$RELEASENAME3"
	fi
	if [ "$snapshot" -eq "1" ] && [ "$nosnapldata" -ne "1" ]; then
	    if [ -e "$OLD_DATA_ARCHIVE.$formatarchive1" ]; then
		mv "$OLD_DATA_ARCHIVE.$formatarchive1" "$PACKAGE3"
	    elif [ -e "$OLD_DATA_ARCHIVE.$formatarchive2" ]; then
		mv "$OLD_DATA_ARCHIVE.$formatarchive1" "$PACKAGE3"
	    else
		common_2 3
	    fi
	fi
	common_2 8 1
	if [ "$cleartemp" -eq "1" ]; then
	    if [ -d "$OLD_DATA_ARCHIVE" ] && [ -f "$OLD_DATA_ARCHIVE.$formatarchive" ]; then
		rm -rf "$OLD_DATA_ARCHIVE"
	    fi
	fi
    else
	echo -en "\nIgnoring $RELEASENAME8 "
	if [ "$snapshot" -eq "1" ]; then common_2 3; fi
    fi
fi

# --------------------------------------------------------

binary_location="$CURRENTDIR/../../os_dependent"
mkrelease_location="$RELEASEDIR1/mk_release"
binary_linux_x86_64="${App_name_s}_e${ENGINE_VERSION}_linux_x86-64.tar.xz"
binary_linux_x86="${App_name_s}_e${ENGINE_VERSION}_linux_x86.tar.xz"
binary_windows_x86="${App_name_s}_e${ENGINE_VERSION}_windows_32-bit.7z"
binary_macos_some="${App_name_s}_e${MAC_ENGINE_VERSION}_macos.tar.xz"

if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ]; then
    download_binary() {
	if [ -f "$binary_location/$1/$2" ] && [ "$(find "$binary_location/$1/$2" -mtime +90)" ]; then
	    cd "$binary_location/$1"; git clean -fdX
	fi
	if [ ! -f "$binary_location/$1/$2" ]; then
	    cd "$binary_location/$1"; git clean -fdX
	    bin_archive="$binary_download_s/$2"; $bin_archive
	    if [ -f "$2" ]; then
		if [ "$(echo "$2" | grep "$formatarchive1")" != "" ]; then
		    tar xJf "$2" -C "./"
		elif [ "$(echo "$2" | grep "$formatarchive2")" != "" ]; then
		    7za x "$2" >/dev/null
		elif [ "$(echo "$2" | grep "$formatarchive3")" != "" ]; then
		    unzip "$2" >/dev/null
		fi
	    fi
	fi
    }
    download_binary "linux/x86-64_binary" "$binary_linux_x86_64"
    download_binary "linux/x86_binary" "$binary_linux_x86"
    download_binary "windows/x86_binary" "$binary_windows_x86"
    download_binary "macos/binary" "$binary_macos_some"
fi

build_binary_dec="yes"; binary_hash_memo="$CURRENTDIR/hash_memory.log"
if [ -f "$binary_hash_memo" ] && [ -d "$RELEASEDIR_ROOT/snapshot" ]; then
    common_3 1
    binary_diff="$(git diff "$(cat "$binary_hash_memo")" HEAD -- ./os_dependent)"
    if [ "$binary_diff" == "" ]; then build_binary_dec="no"; fi
fi

# --------------------------------------------------------

# Linux Binary Archive for a Version Release

if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && [ "$onlywindows_ins" -ne "1" ]; then
  if [ "$snapshot" -eq "1" ] && [ "$build_binary_dec" == "no" ]; then :; else
    common_1 9 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR1/os_dependent/linux/shared/* \
	$mkrelease_location/linux/tools/${App_name_s}-configure-desktop.sh \
	$binary_location/linux/x86-64_binary/* ${!RELEASEDIR}
    rm -f ${!RELEASEDIR}/$binary_linux_x86_64
    if [ -f "${!RELEASEDIR}/glest-dev.ini" ]; then rm -f "${!RELEASEDIR}/glest-dev.ini"; fi
    if [ "$snapshot" -eq "1" ]; then common_2 9 1; fi
    common_1 10 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR1/os_dependent/linux/shared/* \
	$mkrelease_location/linux/tools/${App_name_s}-configure-desktop.sh \
	$binary_location/linux/x86_binary/* ${!RELEASEDIR}
    rm -f ${!RELEASEDIR}/$binary_linux_x86
    if [ -f "${!RELEASEDIR}/glest-dev.ini" ]; then rm -f "${!RELEASEDIR}/glest-dev.ini"; fi
    if [ "$snapshot" -eq "1" ]; then common_2 10 1; fi
  fi
fi

# --------------------------------------------------------

# Linux Archive

if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && [ "$snapshot" -ne "1" ] && [ "$installeronly" -ne "1" ]; then
    common_1 11 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR3/* $RELEASEDIR9/* ${!RELEASEDIR}
    common_2 11 1
    common_1 12 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR3/* $RELEASEDIR10/* ${!RELEASEDIR}
    common_2 12 1
fi

# --------------------------------------------------------

# Windows Binary Archive for a Version Release

if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && [ "$installeronly" -ne "1" ]; then
  if [ "$snapshot" -eq "1" ] && [ "$build_binary_dec" == "no" ]; then :; else
    common_1 14 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR1/os_dependent/windows/shared/* \
	$binary_location/windows/x86_binary/* $mkrelease_location/windows/tools/NetworkThrottleFix.reg \
	${!RELEASEDIR}
    rm -f ${!RELEASEDIR}/$binary_windows_x86
    if [ -f "${!RELEASEDIR}/glest-dev.ini" ]; then rm -f "${!RELEASEDIR}/glest-dev.ini"; fi
    if [ "$snapshot" -eq "1" ]; then common_2 14 1; fi
  fi
fi

# --------------------------------------------------------

# Windows Archive

if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && [ "$snapshot" -ne "1" ] && [ "$installeronly" -ne "1" ]; then
    formatarchive="$formatarchive2"
    common_1 16 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR3/* $RELEASEDIR14/* ${!RELEASEDIR}
    common_2 16 1
    formatarchive="$formatarchive1"
fi

# --------------------------------------------------------

# MacOS Binary Archive for a Version Release

mac_app_name="${App_name_l}.app"
if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && [ "$installeronly" -ne "1" ]; then
  if [ "$snapshot" -eq "1" ] && [ "$build_binary_dec" == "no" ]; then :; else
    common_1 17 no 1
    mkdir -p "${!RELEASEDIR}/$mac_app_name"
    cp -R -f --no-dereference --preserve=all $RELEASEDIR1/os_dependent/macos/shared/* \
	"${!RELEASEDIR}/$mac_app_name"
    cp -R -f --no-dereference --preserve=all $binary_location/macos/binary/* \
	"${!RELEASEDIR}/$mac_app_name/Contents/MacOS"
    if [ "$snapshot" -eq "1" ]; then
	cp -f --no-dereference --preserve=all $mkrelease_location/macos/tools/${App_name_s}-move-snapshot-data.sh \
	    ${!RELEASEDIR}
    fi
    rm -f "${!RELEASEDIR}/$mac_app_name/Contents/MacOS/$binary_macos_some"
    if [ -f "${!RELEASEDIR}/$mac_app_name/Contents/MacOS/glest-dev.ini" ]; then
	rm -f "${!RELEASEDIR}/$mac_app_name/Contents/MacOS/glest-dev.ini"
    fi
    if [ "$snapshot" -eq "1" ]; then common_2 17 1; fi
  fi
fi

# --------------------------------------------------------

# MacOS Archive

if [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && [ "$snapshot" -ne "1" ] && [ "$installeronly" -ne "1" ]; then
    formatarchive="$formatarchive3"
    common_1 19 no 1
    cp -R -f --no-dereference --preserve=all $RELEASEDIR17/* ${!RELEASEDIR}
    cp -R -f --no-dereference --preserve=all $RELEASEDIR3/* \
	"${!RELEASEDIR}/$mac_app_name/Contents/MacOS"
    common_2 19 1
    formatarchive="$formatarchive1"
fi

# --------------------------------------------------------

# Linux Installer for a Version Release

if [ "$snapshot" -ne "1" ] && [ "$binonly" -ne "1" ] && [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && \
    [ "$onlywindows_ins" -ne "1" ]; then
    common_1 30 no 1
    ln -s "$RELEASEDIR1/mk_release/linux/mojosetup/${App_name_s}-installer/${App_name_s}_data.tar.xz" "$RELEASEDIR30/mojosetup"
    cp -R -f --no-dereference --preserve=all $binary_location/linux/$a_binary_dir/* \
	$RELEASEDIR1/os_dependent/linux/$a_binary_dir
    cd "$RELEASEDIR1/mk_release/linux/mojosetup/${App_name_s}-installer/"
    INSTALL_SIZE="$(($(($(du -s "$RELEASEDIR3" | awk '{print $1;}') + \
	$(du -s "$RELEASEDIR9" | awk '{print $1;}'))) * 1000))"
    rm -f ${App_name_l}-installer-${KERNEL}*.run
    echo
    ./make.sh "${!RELEASE}" "$INSTALL_SIZE"
    if [ "$?" -ne "0" ]; then echo 'ERROR: "./make.sh" failed.' >&2; exit 1; fi
    if [ -d "$RELEASEDIR30" ]; then rm -rf "$RELEASEDIR30"; fi
    cp -f --no-dereference --preserve=all ${App_name_l}-installer*.run "$RELEASEDIR_ROOT/${!RELEASE}/"
    rm -f ${App_name_l}-installer*.run
fi

# --------------------------------------------------------

# Windows Installer for a Version Release

if [ "$snapshot" -ne "1" ] && [ "$binonly" -ne "1" ] && [ "$dataonly" -ne "1" ] && [ "$sourceonly" -ne "1" ] && \
    [ "$onlylinux_ins" -ne "1" ] && [ "$(which makensis 2>/dev/null)" != "" ]; then
    common_1 32 no 1
    # ln -s "$RELEASEDIR1/mk_release/windows/nsis" "$RELEASEDIR32/nsis"
    cp -R -f --no-dereference --preserve=all $binary_location/windows/x86_binary/* \
	$RELEASEDIR1/os_dependent/windows/x86_binary
    cd "$RELEASEDIR1/mk_release/windows/nsis/"
    rm -f ${App_name_l}-installer-windows*.exe
    echo
    win32_inst_command="$(which makensis) -DAPVER=${!RELEASE} ${App_name_l}_installer.nsi"
    $win32_inst_command
    if [ "$?" -ne "0" ]; then echo 'ERROR: "makensis" failed.' >&2; fi
    if [ -d "$RELEASEDIR32" ]; then rm -rf "$RELEASEDIR32"; fi
    cp -f --no-dereference --preserve=all ${App_name_l}-installer*.exe "$RELEASEDIR_ROOT/${!RELEASE}/"
    rm -f ${App_name_l}-installer*.exe
fi

# --------------------------------------------------------

common_3 1
if [ "$snapshot" -eq "1" ] && [ "$build_binary_dec" == "yes" ]; then git rev-parse HEAD~ > "$binary_hash_memo"; fi
if [ "$cleartemp" -eq "1" ]; then
    echo -en "\nDeleting temporary directories "
    find "$RELEASEDIR_ROOT/${!RELEASE}/" -name "*${App_name_s}-*" -type d | xargs rm -rf 2>/dev/null
    find "$RELEASEDIR_ROOT/${!RELEASE}/" -name "*${App_name_l}-*" -type d | xargs rm -rf 2>/dev/null
fi
if [ "$create_sum_file" == "yes" ]; then
    cd "$RELEASEDIR_ROOT/${!RELEASE}"
    sum_file_list1="$(find . -maxdepth 1 -name "${App_name_l}-installer*" -not -name "*.sig" -type f | sort -u)"
    sum_file_list2="$(find . -maxdepth 1 -name "${App_name_l}-game*" -not -name "*.sig" -type f | sort -u)"
    sum_file_list="$(echo -e "$sum_file_list1\n$sum_file_list2" | awk -F './' '{print $2}' | xargs)"
    if [ "$(echo "$sum_file_list" | wc -w)" -gt "4" ]; then rm -f *SUM*.txt; fi
    if [ "$(which sha256sum 2>/dev/null)" != "" ]; then
	sum_command="$(which sha256sum) $sum_file_list"
	$sum_command >> SHA256SUM-v${!RELEASE}.txt
    else rm -f SHA256SUM*.txt; fi
fi
sleep 30s
echo
ls -lhA "$RELEASEDIR_ROOT/${!RELEASE}/"

exit 0
