#!/bin/sh
# Use this script to move snapshot data to proper place.
# ----------------------------------------------------------------------------
# 2014 Written by filux <heross(@@)o2.pl>
# Copyright (c) 2014 under GNU GPL v3.0+


# feel free for write something smarter what works on MacOS
cp -R data docs manual maps scenarios techs tilesets Annex.app/Contents/MacOS/
cp glestkeys.ini servers.ini Annex.app/Contents/MacOS/
rm -R data docs manual maps scenarios techs tilesets
rm glestkeys.ini servers.ini
