(function ($) {
	var slideshow = $('.slideshow');

	$('.paging a').live('click', function() {
		var activeSlide = slideshow.find('.active'),
			activeIndex = activeSlide.index();

		pos = ($(this).hasClass('prev') === true) ? activeIndex - 1 :activeIndex + 1; 

		activeSlide.removeClass('active');
		$('.slideshow li:eq(' + pos + ')').addClass('active');
	});

	$('.toc a').live('click', function(e) {
		e.preventDefault();
		$('.slideshow .active').removeClass('active');
		slideshow.find('li img[src="' + $(this).attr('href') + '"]').closest('li').addClass('active');
	});
}(this.jQuery));